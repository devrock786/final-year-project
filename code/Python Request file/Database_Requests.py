import requests
import json
#
#
#
def enterVisitorData(data):

    header = {'content-type': 'application/json'}
    response = requests.post(VISITOR_ADD_URL, headers=header, json=data)

    if(response.status_code == 200):
        print("Data Entered Successfully")
    else:
        print("Error")
#
#
def enterAadharData(data):
    header = {'content-type': 'application/json'}
    response = requests.post(AADHAR_ADD_URL, headers=header, json=data)
    if(response.status_code == 200):
        print("Data Entered Successfully")
    else:
        print("Error")
#
#
def retriveAadharData(uid):
    header = {'content-type': 'application/json'}
    data = {'uid': uid}
    response = requests.post(AADHAR_GET_URL, headers=header, json=data)
    print(response)
    if response.text == "Not returning data":
        return {}
    else:
        data = json.loads(response.text)
        return data
#
#
#
VISITOR_ADD_URL = 'http://127.0.0.1:8000/home/addData/'
AADHAR_ADD_URL = 'http://127.0.0.1:8000/home/addAdharData'
AADHAR_GET_URL = 'http://127.0.0.1:8000/home/retrieve'