from apscheduler.schedulers.blocking import BlockingScheduler
import json
import requests
import datetime
import cv2
import json
import random
import io
# what we need 1. uid,  2. yob, 3. date, 4. guard_name, 5. state, 6. gender

URL = 'http://127.0.0.1:8000/home/addData/'

i = 0
def get_name():
    r = random.randint(0, 1)
    file = 'data_files/male.txt' if r == 0 else 'data_files/female.txt'
    f = open(file)
    name = []
    gender = 'M' if r == 0 else 'F'
    for line in f.readlines():
        name.append(line.strip('\n'))
    return gender, name[random.randint(0, len(name) - 1)]
    f.close()


def yob():
    yob_date = datetime.date.today().year - random.randint(15, 60)
    return yob_date


def get_state():
    file = 'data_files/state.txt'
    f = open(file)
    state = []
    for line in f.readlines():
        state.append(line.strip('\n'))
    return state[random.randint(0, len(state) - 1)]
    f.close()


def get_uid():
    uid = 0
    multiplier = 1
    first = True
    for i in range(0, 12):
        num = random.randint(1, 9) if first == True else random.randint(0, 9)
        uid += multiplier * num
        multiplier *= 10
        first = False
    return uid


def type_of_card():
    label = ['Aadhar Card', 'Pan', 'Driver License']
    return label[random.randint(0, 2)]


def entry_type():
    label = ['Enter', 'exit']
    return label[random.randint(0, 1)]


def make_data_random_year():
    gender, name = get_name()
    data = {'uid': get_uid(),
            'card_type': type_of_card(), 'entry_type': entry_type(),
            'name': name,
            'gender': gender,
            'date': (datetime.datetime.today() - datetime.timedelta(days=random.randint(0, 365))).strftime('%Y-%m-%d %H:%M:%S'), 'state': get_state(), 'yob': yob()}
    return data

def make_random_hour(i):
    gender, name = get_name()
    data = {'uid': get_uid(),
            'card_type': type_of_card(), 'entry_type': entry_type(),
            'name': name,
            'gender': gender,
            'date': (datetime.datetime.now()-datetime.timedelta(seconds=i)).strftime('%Y-%m-%d %H:%M:%S'), 'state': get_state(), 'yob': yob()}
    print(data)
    i+=1
    return data

def make_request(data):
    # print(datetime.now().strftime('%Y/%m/%d %H:%M:%S'))

    # local host URL

    # heder of the request
    header = {'content-type': 'application/json'}

    # data to be send
    new_data = data

    response = requests.post(URL, headers=header, json=new_data)

    if(response.status_code == 200):
        print("Data Entered Successfully")
    else:
        print("Error")
    # if(response.text=="Invalid"):
        # print(response.text)

def request_job():
     make_request(make_data_random_year())


def schedule_job():
     scheduler = BlockingScheduler()
     scheduler.add_job(request_job, 'interval', hours=( 1/ 3600))
     scheduler.start()


request_job()
schedule_job()
