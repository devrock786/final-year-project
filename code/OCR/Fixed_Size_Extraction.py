##############################################################
#                                                            #
#   This program creates cropped images of individual        #
#   characters that will be used to train the neural         #
#   network                                                  #
#                                                            #
#   INPUT  : 1000x1000 image with evenly spaced characters   #
#   OUTPUT : Individual cropped characters of sie 32x32      #
#   AUTHOR : Akshay, Devashish and Mohit                     #
#   DATE   : 10th April 2017                                 #
#                                                            #
##############################################################


import cv2
import math
import numpy as np
import os
import random
import shutil
import time


def rotateImage(image, angle, center = None):

    (h, w) = image.shape[:2]
    if center is None:
        center = (w / 2, h / 2)
    M = cv2.getRotationMatrix2D(center, angle, 1)
    rotated = cv2.warpAffine(image, M, (w, h))
    return rotated
#
#
def translateImage(image, translation):

    rows, cols = image.shape[:2]
    M = np.float32( [[ 1, 0, translation[0]], [ 0, 1, translation[1]]])
    translated = cv2.warpAffine(image, M, (cols, rows) )
    return translated
#
#
def round_down_to_even(f):
    return math.floor(f / 2.0) * 2


random.seed(time.time())

char_set = [
    ['0','1','2','3','4','5','6','7','8','9'],
    ['A','B','C','D','E','F','G','H','I','J'],
    ['K','L','M','N','O','P','Q','R','S','T'],
    ['U','V','W','X','Y','Z'],
    ['a','b','c','d','e','f','g','h','i','j'],
    ['k','l','m','n','o','p','q','r','s','t'],
    ['u','v','w','x','y','z'],
    ['!','#','?','&','\'','"',',','.',':',';'],
    ['@','$','%','_','-','+','=','\\','|','/'],
    ['<','>']
]

data_type = "Testing"
font_list = {
    'Training' : [ 'Arial Bold', 'Arial', 'Calibri Bold', 'Calibri', 'Consolas', 'Corbel', 'Franklin Gothic Bold', 'Open Sans', 'Segoe UI', 'Roboto', 'Raleway'],
    'Testing' : [ 'Helvetica Bold']
} 

shutil.rmtree('code/OCR/'+data_type+' Data')

#Loop Through all the Fonts
for font_name in font_list[data_type]:

    print("Computing character set for "+font_name)

    # Load Image and Covert to Binary Threshold
    img = cv2.imread('code/OCR/Images/Character Set - '+font_name+'.png')
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, 100, 255, cv2.THRESH_BINARY_INV)

    for i in range(len(char_set)):
        for j in range(len(char_set[i])):
            x = j*100 + 15
            y = i*100 + 10
            w = h = 80
            cv2.rectangle(img, (x,y), (x+w,y+h), (50, 50, 250), 2)

            # Crop character and Save as Image
            crop_img =  thresh[y:y+h, x:x+w]
            crop_img = cv2.resize(crop_img, (64, 64), interpolation = cv2.INTER_AREA)
            char_ascii = str(ord(char_set[i][j]))
            directory = 'code/OCR/'+data_type+' Data/'+char_ascii+"/"
            if not os.path.exists(directory):
                os.makedirs(directory)
            
            
            padding = [0, 0, 0, 0]      # Top, Left, Down and Right
            kernel = np.ones((5,3),np.uint8)
            crop_img_dilation = cv2.dilate(crop_img, kernel, iterations = 4)
            #cv2.imshow('Dilation', crop_img_dilation)
            crop_img_erode = cv2.erode(crop_img_dilation, kernel, iterations = 3)
            #cv2.imshow('Eroded', crop_img_erode)

            im2, contours, hierarchy = cv2.findContours(crop_img_erode, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            x, y, w, h = cv2.boundingRect(contours[0])
            #cv2.rectangle(crop_img, (x,y), (x+w,y+h), (128, 128, 128), 2)
            #cv2.imshow('Bounding Box', crop_img)
            #cv2.waitKey(0)
           
            padding[0] = y
            padding[1] = x
            padding[2] = 64 - y - h
            padding[3] = 64 - x - w
            translate = [ int((padding[3]-padding[1])/2), int((padding[2]-padding[0])/2) ]
            padding[0] += translate[1]
            padding[1] += translate[0]
            padding[2] -= translate[1]
            padding[3] -= translate[0]
            crop_img = translateImage(crop_img, translate)


            max_scaleup = min(2,  2*min(padding[1], padding[3])/w , 2*min(padding[0], padding[2])/h)

            crop_img = cv2.resize(crop_img, None, fx=(1+max_scaleup*1/2), fy=(1+max_scaleup*1/2)) 
            (h, w) = crop_img.shape[:2]
            crop_img = crop_img[ int((w-64)/2): int((w-64)/2)+64, int((h-64)/2): int((h-64)/2)+64 ]
            ret, crop_img = cv2.threshold(crop_img, 125, 255, cv2.THRESH_BINARY)

            TRANS_FRAC = 1/3

            # Horizontally Centered
            cv2.imwrite(directory+char_ascii+"_"+font_name+".png", crop_img)

            # Translate Left
            crop_img_trans_left = translateImage(crop_img, [-padding[1]*TRANS_FRAC, 0])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_left.png", crop_img_trans_left)
            # Translate Right
            crop_img_trans_right = translateImage(crop_img, [padding[3]*TRANS_FRAC, 0])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_right.png", crop_img_trans_right)
            # Translate Top
            crop_img_trans_top = translateImage(crop_img, [0, -padding[0]*TRANS_FRAC] )
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_top.png", crop_img_trans_top)
            # Translate Bottom
            crop_img_trans_bottom = translateImage(crop_img, [0, padding[2]*TRANS_FRAC] )
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_bottom.png", crop_img_trans_bottom)

            # Translate Top Left
            crop_img_trans_top_left = translateImage(crop_img, [-padding[1]*TRANS_FRAC, -padding[0]*TRANS_FRAC])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_top_left.png", crop_img_trans_top_left)
            # Translate Top Right
            crop_img_trans_top_right = translateImage(crop_img, [padding[3]*TRANS_FRAC, -padding[0]*TRANS_FRAC])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_top_right.png", crop_img_trans_top_right)
            # Translate Bottom Right
            crop_img_trans_bottom_right = translateImage(crop_img, [padding[3]*TRANS_FRAC,  padding[2]*TRANS_FRAC])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_bottom_right.png", crop_img_trans_bottom_right)
            # Translate Bottom Left
            crop_img_trans_bottom_left = translateImage(crop_img, [-padding[1]*TRANS_FRAC,  padding[2]*TRANS_FRAC])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_trans_bottom_left.png", crop_img_trans_bottom_left)

            # Rotate Positive Angle
            crop_img_rot_pos = rotateImage(crop_img, random.randint(3, 6))
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_pos.png", crop_img_rot_pos)
            # Rotate Positive Angle Translate Left
            crop_img_rot_pos_trans_left = translateImage(crop_img_rot_pos, [-padding[1]*TRANS_FRAC, 0])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_pos_trans_left.png", crop_img_rot_pos_trans_left)
            # Rotate Positive Angle Translate Right
            crop_img_rot_pos_trans_right = translateImage(crop_img_rot_pos, [padding[3]*TRANS_FRAC, 0])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_pos_trans_right.png", crop_img_rot_pos_trans_right)
            # Rotate Positive Angle Translate Top
            crop_img_rot_pos_trans_top = translateImage(crop_img_rot_pos, [0, -padding[0]*TRANS_FRAC] )
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_pos_trans_top.png", crop_img_rot_pos_trans_top)
            # Rotate Positive Angle Translate Bottom
            crop_img_rot_pos_trans_bottom = translateImage(crop_img_rot_pos, [0, padding[2]*TRANS_FRAC] )
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_pos_trans_bottom.png", crop_img_rot_pos_trans_bottom)

            # Rotate Negative Angle
            crop_img_rot_neg = rotateImage(crop_img, random.randint(-6, -3))
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_neg.png", crop_img_rot_neg)
            # Rotate Negative Angle Translate Left
            crop_img_rot_neg_trans_left = translateImage(crop_img_rot_neg, [-padding[1]*TRANS_FRAC, 0])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_neg_trans_left.png", crop_img_rot_neg_trans_left)
            # Rotate Negative Angle Translate Right
            crop_img_rot_neg_trans_right = translateImage(crop_img_rot_neg, [padding[3]*TRANS_FRAC, 0])
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_neg_trans_right.png", crop_img_rot_neg_trans_right)
            # Rotate Negative Angle Translate Top
            crop_img_rot_neg_trans_top = translateImage(crop_img_rot_neg, [0, -padding[0]*TRANS_FRAC] )
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_neg_trans_top.png", crop_img_rot_neg_trans_top)
            # Rotate Negative Angle Translate Bottom
            crop_img_rot_neg_trans_bottom = translateImage(crop_img_rot_neg, [0, padding[2]*TRANS_FRAC] )
            cv2.imwrite(directory+char_ascii+"_"+font_name+"_rot_neg_trans_bottom.png", crop_img_rot_neg_trans_bottom)

        