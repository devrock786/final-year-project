##############################################################
#                                                            #
#   This program                                             #
#                                                            #
#                                                            #
#   INPUT  : An image with list of well spaced characters    #
#   OUTPUT : Bounding Box around the characters              #
#   AUTHOR : Akshay, Devashish and Mohit                     #
#   DATE   : 9th April 2017                                  #
#                                                            #
##############################################################
import cv2
import datetime
import glob
import os
import PIL
import random
import traceback

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
#
#
#
def getData(dir):

    data_files = []
    file_list =  sorted( glob.glob(dir+'*.png'))
    for file_name in file_list:
        data_files.append( file_name)

    return data_files
#
#    
def getImage(fileList):
    imageList = [np.expand_dims(cv2.imread(fileName, cv2.IMREAD_GRAYSCALE), 2) for fileName in fileList]
    return imageList
#
#
def conv2d(x, filter_weights, stride):
    # stride = [batch, height, width, channels]
    return tf.nn.conv2d(input=x, filter=filter_weights, strides=[1, stride, stride, 1], padding='SAME')
#
#
def maxpool2d(x, stride):
    # stride = [batch, height, width, channels]
    # ksize = [batch, height, width, channels]
    return tf.nn.max_pool(value=x, ksize=[1, stride, stride, 1], strides=[1, stride, stride, 1], padding='SAME')
#
#
def relu(x):
    return tf.nn.relu(x)
#
#
def neural_network_model():
    
    # Weights
    with tf.name_scope('Weights'):
        # Variables
        weights_conv1 = tf.Variable(tf.random_normal([7, 7, 1, 32]) , name="Weights_Conv1")
        weights_conv2 = tf.Variable(tf.random_normal([5, 5, 32, 48]), name="Weights_Conv2")
        weights_conv3 = tf.Variable(tf.random_normal([3, 3, 48, 64]), name="Weights_Conv3")
        weights_conv4 = tf.Variable(tf.random_normal([3, 3, 64, 96]), name="Weights_Conv4")
        weights_fc5 = tf.Variable(tf.random_normal([96, 1024]), name="Weights_FC5")
        weights_out = tf.Variable(tf.random_normal([1024, output_features]), name="Weights_Out")
        # Summaries
        tf.summary.histogram('Weights Conv1', weights_conv1)
        tf.summary.histogram('Weights Conv2', weights_conv2)
        tf.summary.histogram('Weights Conv3', weights_conv3)
        tf.summary.histogram('Weights Conv4', weights_conv4)
        tf.summary.histogram('Weights FC5', weights_fc5)
        tf.summary.histogram('Weights Out', weights_out)

    # Biases
    with tf.name_scope('Biases'):
        # Variables  
        biases_conv1 = tf.Variable(tf.random_normal([32]), name="Biases_Conv1")
        biases_conv2 = tf.Variable(tf.random_normal([48]), name="Biases_Conv2")
        biases_conv3 = tf.Variable(tf.random_normal([64]), name="Biases_Conv3")
        biases_conv4 = tf.Variable(tf.random_normal([96]), name="Biases_Conv4")
        biases_fc5 = tf.Variable(tf.random_normal([1024]), name="Biases_FC5")
        biases_out = tf.Variable(tf.random_normal([output_features]), name="Biases_Out")
        # Summaries
        tf.summary.histogram('Biases Conv1', biases_conv1)
        tf.summary.histogram('Biases Conv2', biases_conv2)
        tf.summary.histogram('Biases Conv3', biases_conv3)
        tf.summary.histogram('Biases Conv4', biases_conv4)
        tf.summary.histogram('Biases FC5', biases_fc5)
        tf.summary.histogram('Biases Out', biases_out)

    # Neural Network
    tf.summary.image('1_Input_Images', input_placeholder)
    with tf.name_scope('Convolution_Layer_1'):
        conv1 = tf.add(conv2d(input_placeholder, weights_conv1, 1), biases_conv1)
        tf.summary.image('Input_Images Conv1', tf.reshape(conv1 , shape=[-1, 64*8, 64*4, 1]))
        relu1 = relu(conv1)
    with tf.name_scope('Pooling_Layer_1'):
        pool1 = maxpool2d(relu1, 4)
        tf.summary.image('Input_Images Pool1', tf.reshape(pool1 , shape=[-1, 16*8, 16*4, 1]))

    with tf.name_scope('Convolution_Layer_2'):
        conv2 = tf.add(conv2d(pool1, weights_conv2, 1), biases_conv2)
        tf.summary.image('Input_Images Conv2', tf.reshape(conv2 , shape=[-1, 16*8, 16*6, 1]))
        relu2 = relu(conv2)
    with tf.name_scope('Pooling_Layer_2'):
        pool2 = maxpool2d(relu2, 2)
        tf.summary.image('Input_Images Pool2', tf.reshape(pool2 , shape=[-1, 8*8, 8*6, 1]))

    with tf.name_scope('Convolution_Layer_3'):
        conv3 = tf.add(conv2d(pool2, weights_conv3, 1), biases_conv3)
        tf.summary.image('Input_Images Conv3', tf.reshape(conv3 , shape=[-1, 8*8, 8*8, 1]))
        relu3 = relu(conv3)
    with tf.name_scope('Pooling_Layer_3'):
        pool3 = maxpool2d(relu3, 2)
        tf.summary.image('Input_Images Pool3', tf.reshape(pool3 , shape=[-1, 4*8, 4*8, 1]))

    with tf.name_scope('Convolution_Layer_4'):
        conv4 = tf.add(conv2d(pool3, weights_conv4, 1), biases_conv4)
        tf.summary.image('Input_Images Conv4', tf.reshape(conv4 , shape=[-1, 4*12, 4*8, 1]))
        relu4 = relu(conv4)
    with tf.name_scope('Pooling_Layer_4'):
        pool4 = maxpool2d(relu4, 4)
        tf.summary.image('Input_Images Pool4', tf.reshape(pool4 , shape=[-1, 1*12, 1*8, 1]))
        
    with tf.name_scope('Fully_Connected_Layer_5'):
        pool4 = tf.reshape(pool4 , shape=[-1, 96])
        fc5 = tf.add(tf.matmul(pool4,weights_fc5), biases_fc5)

    with tf.name_scope('Output_Layer'):
        output = tf.add(tf.matmul(fc5, weights_out), biases_out)
        output = tf.reshape(output , shape=[-1, output_features])
        return output
#
#
def predictCharacter(test_image):
    
    test_image = np.expand_dims(test_image, 3)
    
    with tf.Session() as sess :
        with tf.device("/cpu:0"):
            saver = tf.train.Saver()
            saver.restore(sess, os.path.join(LOG_DIR, 'latest-model.ckpt'))
            # Predict Test Data
            prediction_index = sess.run([prediction], feed_dict={input_placeholder: test_image} )
            result = [char_set[i]  for i in prediction_index[0]]
            return result
#
#
#
def initGlobals(char_set_id):

    global output_features
    global input_features
    global input_placeholder
    global output_placeholder
    global prediction
    global char_set
    global LOG_DIR

    tf.reset_default_graph()
    
    if char_set_id == 0:
        char_set = card_set
        LOG_DIR = "code/OCR/Tensorflow Logs/Card/"
    elif char_set_id == 1:
        char_set = alphabet_set
        LOG_DIR = "code/OCR/Tensorflow Logs/Alphabets/"
    elif char_set_id == 2:
        char_set = uppercase_alphabet_set
        LOG_DIR = "code/OCR/Tensorflow Logs/Uppercase Alphabets/"
    elif char_set_id == 3:
        char_set = digit_set
        LOG_DIR = "code/OCR/Tensorflow Logs/Digits/"
    else:
        LOG_DIR = "code/OCR/Tensorflow Logs/Unknown"


    input_features = [None, SIZE, SIZE, 1]
    output_features = len(char_set)
    with tf.name_scope('Inputs'):
        input_placeholder = tf.placeholder('float', input_features, name='image_input')
        output_placeholder = tf.placeholder('float', [None, output_features], name='label_input')
    
    output_prediction = neural_network_model()
    with tf.name_scope('Prediction'):
        prediction = tf.argmax(output_prediction, 1)
#
#
#
card_set = [
    '0','1','2','3','4','5','6','7','8','9',
    'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z',
    '#','?','&','@','\\','/', '-', ',', '.'
]

alphabet_set = [
    'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z',
    'a','b','c','d','e','f','g','h','i','j',
    'k','l','m','n','o','p','q','r','s','t',
    'u','v','w','x','y','z'
]

uppercase_alphabet_set = [
    'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z'
]

digit_set = [
    '0','1','2','3','4','5','6','7','8','9',
]



SIZE = 64
BATCH_SIZE = 128

