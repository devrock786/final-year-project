##############################################################
#                                                            #
#   This program creates a bounding box around the           #
#   characterset image.                                      #
#                                                            #
#   INPUT  : An image with list of well spaced characters    #
#   OUTPUT : Bounding Box around the characters              #
#   AUTHOR : Akshay, Devashish and Mohit                     #
#   DATE   : 9th April 2017                                  #
#                                                            #
##############################################################


import cv2
import numpy as np


img_list = [cv2.imread('code/OCR/Training Data/65/65_Arial Bold.png'), cv2.imread('code/OCR/Training Data/65/65_Arial.png'),
        cv2.imread('code/OCR/Training Data/65/65_Consolas.png'), cv2.imread('code/OCR/Training Data/65/65_Corbel.png'),
        cv2.imread('code/OCR/Training Data/65/65_Impact.png'), cv2.imread('code/OCR/Training Data/65/65_Segoe UI.png')]

# Convert Image to Binary and Dilate it
for i, img in enumerate(img_list):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, 125, 255, cv2.THRESH_BINARY)

    kernel = np.ones((2,2),np.uint8)
    morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((2, 2),np.uint8)
    morph = cv2.erode(morph, kernel, iterations = 1)
    kernel = np.ones((2, 2),np.uint8)
    morph = cv2.dilate(morph, kernel, iterations = 2)


    cv2.imshow('Image '+str(i), cv2.resize(morph, None, fx=4, fy=4, interpolation=cv2.INTER_NEAREST))
    cv2.imshow('Thresh '+str(i), cv2.resize(img_gray, None, fx=4, fy=4, interpolation=cv2.INTER_NEAREST))
cv2.waitKey(0)