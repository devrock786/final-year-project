##############################################################
#                                                            #
#   This program will send an OTP to the given phone number  #
#                                                            #
#   INPUT  : OTP and Mobile Number                           #
#   OUTPUT : Sends an SMS to the number using MSG91 API      #
#   AUTHOR : Akshay, Devashish and Mohit                     #
#   DATE   : 15th May 2017                                   #
#                                                            #
##############################################################
import requests
#
#
#
def sendOTP(OTP, mobile_number):
    base_url = "https://control.msg91.com/api/sendhttp.php"
    args = {
        'authkey': '152544AILThlMh4Z5919b18d', 
        'mobiles': mobile_number,
        'message': 'Your OTP is '+str(OTP),
        'sender' : 'SCAUTH',
        'route' : 4
    }

    response  = requests.post(base_url, data=args)

    return response
#
#