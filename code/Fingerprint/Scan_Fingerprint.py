import os
import cv2
import random
import math
import numpy as np
import matplotlib.pyplot as plt
#
#
#
def ImagetoString(image):
    string = ""
    for i in range(len(image)):
        for j in range(len(image[i])):
            string+=str(image[i][j])+","
    return string[:-1]
#
#
def StringtoImage(img_str):
    img_list = img_str.split(',')
    image = np.array(img_list, dtype=np.uint8).reshape((IMG_HEIGHT, IMG_WIDTH))
    return image
#
#
def scanFingerprint():
    os.system('java -jar "code/Fingerprint/fingerprint.jar" "code/Fingerprint/Scanned Images"')

#
#
def fingerprintSimilarity(image1, image2):
    global DEBUG

    # _, image1 =  cv2.threshold(image1, 70, 255, cv2.THRESH_BINARY)
    # _, image2 =  cv2.threshold(image2, 50, 255, cv2.THRESH_BINARY)

    img1 = image1.copy()
    img2 = image2.copy()

    # Initiate ORB detector
    orb = cv2.ORB_create()

    # find the keypoints and descriptors with ORB
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)

    # create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    # Match descriptors.
    matches = bf.match(des1, des2)
    # Sort them in the order of their distance.
    matches = sorted(matches, key = lambda x:x.distance)
    
    n = 10
    img3 = cv2.drawMatches(img1, kp1, img2, kp2, matches[:n], None, matchColor=(20, 200, 20), flags=2)
    
    dist = []
    for mat in matches[:n]:
        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx
        (x1, y1) = kp1[img1_idx].pt
        (x2, y2) = kp2[img2_idx].pt
        dist.append(math.sqrt( (x1-x2)**2 + (y1-y2)**2 ))

    mean = np.mean(dist)
    abs_dev = []
    for i in dist:
        abs_dev.append(abs(dist-mean))

    if DEBUG:
        plt.imshow(img3),plt.show()
    return 100 - 100*np.mean(abs_dev)/math.sqrt(IMG_WIDTH**2 + IMG_HEIGHT**2)
#
#
#
IMG_WIDTH = 264
IMG_HEIGHT = 324
DEBUG = False
if __name__ == "__main__":
    DEBUG = True
    scanFingerprint()
    img1 = cv2.imread('code/Fingerprint/Scanned Images/Akshay_LEFT_INDEX.png', cv2.IMREAD_COLOR)
    img2 = cv2.imread('code/Fingerprint/Scanned Images/fingerprint.png', cv2.IMREAD_COLOR)
    fingerprintSimilarity(img1, img2)

