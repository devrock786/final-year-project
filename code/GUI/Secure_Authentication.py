import sys
sys.path.append("code/OCR")
sys.path.append("code/OTP")
sys.path.append("code/QR Code")
sys.path.append("code/Fingerprint")
sys.path.append("code/Python Request file")

import datetime
import random
import cv2
from tkinter import *
from tkinter import filedialog, font
from PIL import Image, ImageTk

import Scan_Fingerprint as FP
import Drivers_License_Data as DL
import QR_Scanner as QR
import Database_Requests as DB
import Send_OTP as OTP
import Database_Requests as rt

#
#
#
def resetCardType():
    global cardType
    aadhar_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
    dl_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
    cardType = None
#
#
def resetGenderType():
    global genderType
    male_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
    female_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
    genderType = None
#
#
def resetAuthType():
    global authType
    none_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
    otp_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
    biometric_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
    authType = None
#
#
def submitData(event):
    data = {
        'entry_type': 'Entry',
        'card_type' : cardType,
        'uid' : id_entry.get(),
        'name' : name_entry.get(),
        'state' : loc_entry.get(),
        'date' : datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'yob' : dob_entry.get()[-4:],
        'gender': genderType[0],
    }
    rt.enterVisitorData(data)


#
#
def generateOTP(event):
    global curOTP

    aadhar_record = DB.retriveAadharData(id_entry.get())
    if 'phone_number' in aadhar_record:
        curOTP = random.randint(1000, 9999)
        print(curOTP)
        OTP.sendOTP(curOTP, aadhar_record['phone_number'])
    else:
        print("Invalid Aadhar ID")
#
#
def checkOTP(event):
    global validAuth

    if curOTP!=None:
        if str(curOTP) == otp_entry.get():
            otp_status_label.configure(text="Valid OTP")
            validAuth = True
        else:
            otp_status_label.configure(text="Invalid OTP")
            validAuth = False
    else:
        print("OTP not generated")
#
#
def compareFingerprintWithDatabase():

    dbImage = scanImage = None

    aadhar_record = DB.retriveAadharData(id_entry.get())
    if 'fingerprint' in aadhar_record:
        dbImage = FP.StringtoImage(aadhar_record['fingerprint'])
    else:
        print("Invalid Aadhar ID")
        return False
    
    if fingerprintImagePath!=None or fingerprintImagePath!="":
        scanImage = cv2.imread(fingerprintImagePath, cv2.IMREAD_GRAYSCALE)
    else:
        print("No scanned fingerprint")
        return False

    match_percent = FP.fingerprintSimilarity(scanImage, dbImage)
    print("\nMatch Percentage :", match_percent, "\n")
    if match_percent > 90:
        return True
    else:
        return False
#
#
def scanFingerprint(event):
    global fingerprintImagePath
    global fingerprintImage
    global validAuth

    FP.scanFingerprint()
    fingerprintImagePath = "code/Fingerprint/Scanned Images/fingerprint.png"
    fingerprintImage = Image.open(fingerprintImagePath)
    fingerprintImage.thumbnail((132, 162))
    fingerprintImage = ImageTk.PhotoImage(fingerprintImage)
    fingerprint_img_label.configure(image=fingerprintImage)
    fingerprint_status_label.configure(text="Verifying...")
    root.update()
    match = compareFingerprintWithDatabase()
    if match:
        fingerprint_status_label.configure(text="Valid Match")
        validAuth = True
    else:
        fingerprint_status_label.configure(text="Invalid Match")
        validAuth = False
#
#
def chooseImage(event):
    global cardImage
    global cardImagePath
    filename = filedialog.askopenfilename(filetypes = [("PNG", ".png"), ("JPEG", ".jpg")] )
    if filename!="":
        cardImagePath = filename
        cardImage = Image.open(cardImagePath)
        cardImage.thumbnail((500, 500))
        cardImage = ImageTk.PhotoImage(cardImage)
        cardImage_label.configure(image=cardImage)
        resetAuthType()
        resetGenderType()
        resetCardType()
        
#
#
def hideAuthentication():

    # Hide OTP Authentication
    generate_otp_button.configure(fg=WHITE, bg=WHITE)
    generate_otp_button.place(height=0)
    otp_entry.delete(0, END)
    otp_entry.place(height=0)
    check_otp_button.configure(fg=WHITE, bg=WHITE)
    check_otp_button.place(height=0)
    otp_status_label.configure(text="")
    otp_status_label.place(height=0)

    # Hide Biometric Authentication
    scan_button.configure(fg=WHITE, bg=WHITE)
    scan_button.place(height=0)
    fingerprint_img_label.place(height=0)
    fingerprint_status_label.configure(text="")
    fingerprint_status_label.place(height=0)
    fingerprint_img_label.configure(image=None)
    fingerprintImagePath = None
    fingerprintImage = None
#
#
def toggleCardType(event):
    global cardType
    global cardImagePath

    if cardImagePath != None:
        resetCardType()
        event.widget.configure(bg=BUTTON_BG, fg=BUTTON_FG)
        cardType = event.widget['text']
        
        none_button.configure(bg=BUTTON_BG, fg=BUTTON_FG)
        otp_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
        biometric_button.configure(bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
        hideAuthentication()
        authType = "None"
        root.update()

        if cardType == "Driver License":
            data = DL.getData(cardImagePath)
            if bool(data):
                id_entry.delete(0, END)
                id_entry.insert(0, "".join(data['id']))
                name_entry.delete(0, END)
                name_entry.insert(0, "".join(data['name']))
                dob_entry.delete(0, END)
                dob_entry.insert(0, "".join(data['dob']))
                loc_entry.delete(0, END)
                loc_entry.insert(0, "".join(data['state']))
        elif cardType == 'Aadhar Card':
            data = QR.getQRCodeData(cardImagePath)
            if  bool(data):
                id_entry.delete(0, END)
                id_entry.insert(0, "".join(data['uid']))
                name_entry.delete(0, END)
                name_entry.insert(0, "".join(data['name']))
                dob_entry.delete(0, END)
                dob_entry.insert(0, "".join(data['dob']))
                loc_entry.delete(0, END)
                loc_entry.insert(0, "".join(data['state']))
    else:
        print("No File Choosen")
#
#
def toggleGender(event):
    global genderType
    resetGenderType()
    event.widget.configure(bg=BUTTON_BG, fg=BUTTON_FG)
    genderType = event.widget['text']

#
#
def toggleAuth(event):
    global authType

    if cardType=="Aadhar Card":
        resetAuthType()
        event.widget.configure(bg=BUTTON_BG, fg=BUTTON_FG)
        authType = event.widget['text']

        hideAuthentication()

        if authType == "OTP":
            generate_otp_button.configure(fg=BUTTON_FG, bg=BUTTON_BG)
            generate_otp_button.place(height=BUTTON_HEIGHT)
            otp_entry.place(height=ENTRY_HEIGHT)
            check_otp_button.configure(fg=BUTTON_FG, bg=BUTTON_BG)
            check_otp_button.place(heigh =BUTTON_HEIGHT)
            otp_status_label.place(height=ENTRY_HEIGHT)
        elif authType == "Biometric":
            scan_button.configure(fg=BUTTON_FG, bg=BUTTON_BG)
            scan_button.place(height=BUTTON_HEIGHT)
            fingerprint_img_label.place(height=162)
            fingerprint_status_label.place(height=LABEL_HEIGHT)
#
#
#

# GLOBAL CONSTANTS
YELLOW_A400 = "#FFFF00"
LIGHT_BLUE_300 = "#4FC3F7"
BLUE_500 = "#2196F3"

WHITE = "#FFFFFF"
GREY_50  = "#FAFAFA"
GREY_100 = "#F5F5F5"
GREY_200 = "#EEEEEE"
GREY_300 = "#E0E0E0"
GREY_400 = "#BDBDBD"
GREY_500 = "#9E9E9E"
GREY_600 = "#757575"
GREY_700 = "#616161"
GREY_800 = "#424242"
GREY_900 = "#212121"
BLACK = "#000000"


BUTTON_HEIGHT = 40
LABEL_HEIGHT = 20
ENTRY_HEIGHT = 40
LABLE_FG = BLUE_500

BUTTON_BG = YELLOW_A400
BUTTON_FG = GREY_800
BUTTON_INACTIVE_BG = GREY_300
BUTTON_INACTIVE_FG = GREY_500


cardImage = None
cardImagePath = None
fingerprintImage = None
fingerprintImagePath = None
cardType = None
genderType = None
authType = "None"
curOTP = None
validAuth = False

root = Tk()
root.title("Secure Authentication and Tracking")
root.geometry('1280x720')
root.configure(bg=WHITE)

root.tk_setPalette(background=WHITE, foreground=GREY_800, activeBackground=WHITE, activeForeground=GREY_800)

root.resizable(0, 0)

default_font = font.nametofont("TkDefaultFont")
default_font.configure(size=14, weight="bold")

cardImage_label = Label(root, borderwidth=1, relief="ridge")
cardImage_label.place(x=10, y=10, height=500, width = 500)

browse_button = Button(root, text="Browse", bg=BUTTON_BG, fg=BUTTON_FG)
browse_button.bind('<Button-1>', chooseImage)
browse_button.place(x = 10, y=520, height = BUTTON_HEIGHT)


# Card Type 
card_label = Label(root, text = "Choose Card Type", fg=LABLE_FG)
card_label.place(x = 530, y=10, height = LABEL_HEIGHT)

aadhar_button = Button(root, text="Aadhar Card", bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
aadhar_button.bind('<Button-1>', toggleCardType)
aadhar_button.place(x = 530, y=40, height = BUTTON_HEIGHT)
root.update()

dl_button = Button(root, text="Driver License", bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
dl_button.bind('<Button-1>', toggleCardType)
dl_button.place(x = 530+aadhar_button.winfo_width()+10, y=40, height = BUTTON_HEIGHT)



# ID No
id_label = Label(root, text = "ID No.", fg=LABLE_FG)
id_label.place(x = 530, y=100, height = LABEL_HEIGHT)

id_entry = Entry(root, font=("TkDefaultFont", 14), bd=1, relief="ridge")
id_entry.place(x = 530, y=130, height = ENTRY_HEIGHT)


# Name 
name_label = Label(root, text = "Name", fg=LABLE_FG)
name_label.place(x = 900, y=100, height = LABEL_HEIGHT)

name_entry = Entry(root, font=("TkDefaultFont", 14), bd=1, relief="ridge")
name_entry.place(x = 900, y=130, height = ENTRY_HEIGHT)


# Date of Birth 
dob_label = Label(root, text = "Date of Birth", fg=LABLE_FG)
dob_label.place(x = 530, y=190, height = LABEL_HEIGHT)

dob_entry = Entry(root, font=("TkDefaultFont", 14), bd=1, relief="ridge")
dob_entry.place(x = 530, y=220, height = ENTRY_HEIGHT)


# Location 
loc_label = Label(root, text = "Location", fg=LABLE_FG)
loc_label.place(x = 900, y=190, height = LABEL_HEIGHT)

loc_entry = Entry(root, font=("TkDefaultFont", 14), bd=1, relief="ridge")
loc_entry.place(x = 900, y=220, height = ENTRY_HEIGHT)

# Gender Type 
gender_label = Label(root, text = "Gender", fg=LABLE_FG)
gender_label.place(x = 530, y=280, height = LABEL_HEIGHT)

male_button = Button(root, text="Male", bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
male_button.bind('<Button-1>', toggleGender)
male_button.place(x = 530, y=310, height = BUTTON_HEIGHT)
root.update()

female_button = Button(root, text="Female", bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
female_button.bind('<Button-1>', toggleGender)
female_button.place(x = 530+male_button.winfo_width()+10, y=310, height = BUTTON_HEIGHT)

#Authentication
auth_label = Label(root, text = "Authentication", fg=LABLE_FG)
auth_label.place(x = 530, y=370, height = LABEL_HEIGHT)

none_button = Button(root, text="None", bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
none_button.bind('<Button-1>', toggleAuth)
none_button.place(x = 530, y=400, height = BUTTON_HEIGHT)
root.update()

otp_button = Button(root, text="OTP", bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
otp_button.bind('<Button-1>', toggleAuth)
otp_button.place(x = 530+none_button.winfo_width()+10, y=400, height = BUTTON_HEIGHT)
root.update()

biometric_button = Button(root, text="Biometric", bg=BUTTON_INACTIVE_BG, fg=BUTTON_INACTIVE_FG)
biometric_button.bind('<Button-1>', toggleAuth)
biometric_button.place(x = 530+none_button.winfo_width()+otp_button.winfo_width()+20, y=400, height = BUTTON_HEIGHT)

# OPT Authentication
generate_otp_button = Button(root, text="Generate OTP", bg=WHITE, fg=WHITE)
generate_otp_button.bind('<Button-1>', generateOTP)
generate_otp_button.place(x = 530, y=450, height = 0)
root.update()

otp_entry = Entry(root, font=("TkDefaultFont", 14), bd=1, relief="ridge")
otp_entry.place(x = 530+generate_otp_button.winfo_width()+10, y=450, height = 0)

check_otp_button = Button(root, text="Check OTP", bg=WHITE, fg=WHITE)
check_otp_button.bind('<Button-1>', checkOTP)
check_otp_button.place(x = 530, y=500, height = 0)
root.update()

otp_status_label = Label(root, text = "", fg=LABLE_FG)
otp_status_label.place(x = 530+check_otp_button.winfo_width()+10, y=500, height = 0)


# Biometric Authentication
scan_button = Button(root, text="Scan Fingerprint", bg=WHITE, fg=WHITE)
scan_button.bind('<Button-1>', scanFingerprint)
scan_button.place(x = 530, y=450, height = 0)
root.update()

fingerprint_img_label = Label(root, borderwidth=1, relief="ridge")
fingerprint_img_label.place(x = 530+scan_button.winfo_width()+20, y=450, width=132, height = 0)

fingerprint_status_label = Label(root, text = "", fg=LABLE_FG)
fingerprint_status_label.place(x = 530, y=500, height = 0)


#Submit Button
submit_button = Button(root, text="Submit")
submit_button.bind('<Button-1>', submitData)
submit_button.place(x = 530, y=600, height = BUTTON_HEIGHT)

root.mainloop()
