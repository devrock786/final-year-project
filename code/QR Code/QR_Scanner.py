
import pyzbar.pyzbar as pyzbar
import cv2
#
#
#
def parseAadharXMlString(xml_str):

    xml_str = xml_str.split('<')[2][23:-2]
    result = {}
    state = 'key'
    key = ""
    value = ""
    for c in xml_str:

        if c == '"':
            if state == 'key':
                state='value'
            else :
                state='key'
                result[key[:-1]] = value
                key=value=""
        elif c == ' ' and state=='key':
            pass
        else:
            if state == 'key':
                key+=c
            elif state == 'value':
                value+=c

    return result
#
#
def getQRCodeData(filepath):
    img = cv2.imread(filepath)
    qr_data = pyzbar.decode(img)
    if len(qr_data)==0:
        print("No QR Found")
        return {}
    xml_str = str(qr_data[0].data)[2:-1]
    result = parseAadharXMlString(xml_str)

    if ('yob' in result) and ('dob' not in result):
        result['dob'] = '01/07/'+result['yob']
    elif 'dob' in result:
        result['dob'] = result['dob'][8:10]+"/"+result['dob'][5:7]+"/"+result['dob'][0:4]

    return result
#
#
#
if __name__ == "__main__":
    res = getQRCodeData("code/ID Cards/Aadhar 1.png")
    print("Result = ", res)
    


