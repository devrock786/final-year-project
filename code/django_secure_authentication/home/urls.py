from django.conf.urls import url
from django.views.generic import ListView
from home.models import Visitors
from django.db.models import Count
from . import views
urlpatterns = [

    ##Retrieve data
    url(r'/retrieve',views.retreiveData),
    url(r'/addAdharData',views.addAdharData),

    url(r'^$', views.index),
    # to display weekly data
    url(r'^/week', ListView.as_view(queryset=Visitors.objects.raw("Select date(visiting_date) as id, count(date(visiting_date)) \
    as count_date from home_visitors \
    where date(visiting_date)>date('now','-7 days') group by date(visiting_date) order by visiting_date"), template_name="home/week.html")),

    # to display number of visitors from each state
    url(r'^/state', ListView.as_view(queryset=Visitors.objects.raw("Select LOWER(state) as id, count(LOWER(state)) \
    as count_data from home_visitors where date(visiting_date)>date('now','-7 days') \
    group by LOWER(state) order by count_data desc LIMIT 15 "), template_name="home/state.html")),

    # getting the Gender
    url(r'^/gender', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(gender) as gender_count, gender \
    from home_visitors  where date(visiting_date)>date('now','-7 days')\
    group by gender order by visiting_date"), template_name="home/gender.html")),

    # to add data
    url(r'addData', views.addData),

    # getting Visitors
    url(r'^/visitor', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(identity_no) as total \
    from home_visitors  where date(visiting_date)>date('now','-7 day')\
    group by identity_no order by visiting_date"), template_name="home/unique_visitor.html")),

    # getting all visitors for age group
    url(r'^/ageGroupWeek', ListView.as_view(queryset=Visitors.objects.raw("Select id, year_of_birth,count(year_of_birth) as total \
    from home_visitors  where date(visiting_date)>date('now','-7 days')\
    group by year_of_birth order by visiting_date"), template_name="home/ageGroup.html")),

    # card Type
    url(r'^/cardType', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(type_of_id) as card_count, type_of_id as card \
    from home_visitors  where date(visiting_date)>date('now','-7 days')\
    group by type_of_id order by visiting_date"), template_name="home/cardType.html")),


    ##########################################################################
    # adding URL for monthly Data
    # to display weekly data
    url(r'^/month', ListView.as_view(queryset=Visitors.objects.raw("Select id,strftime('%m',visiting_date) as m, count(strftime('%m',visiting_date)) as c from home_visitors where visiting_date > date('now','-12 months')\
     group by strftime('%m',visiting_date) order by visiting_date"), template_name="home/month.html")),

    # to display number of visitors from each state
    url(r'^/MstateMonth', ListView.as_view(queryset=Visitors.objects.raw("Select LOWER(state) as id, count(LOWER(state)) \
    as count_data from home_visitors where date(visiting_date)>date('now','-12 Months') \
    group by LOWER(state) order by count_data desc LIMIT 15 "), template_name="home/state.html")),

    # getting the Gender
    url(r'^/MgenderMonth', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(gender) as gender_count, gender \
    from home_visitors  where date(visiting_date)>date('now','-12 Months')\
    group by gender order by visiting_date"), template_name="home/gender.html")),

    # getting Visitors
    url(r'^/MvisitorMonth', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(identity_no) as total \
    from home_visitors  where date(visiting_date)>date('now','-12 Months')\
    group by identity_no order by visiting_date"), template_name="home/unique_visitor.html")),

    # getting all visitors for age group
    url(r'^/MageGroupMonth', ListView.as_view(queryset=Visitors.objects.raw("Select id, year_of_birth,count(year_of_birth) as total \
    from home_visitors  where date(visiting_date)>date('now','-12 months')\
    group by year_of_birth order by visiting_date"), template_name="home/ageGroup.html")),


    # card Type
    url(r'^/McardTypeMonth', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(type_of_id) as card_count, type_of_id as card \
    from home_visitors  where date(visiting_date)>date('now','-12 months')\
    group by type_of_id order by visiting_date"), template_name="home/cardType.html")),

    ##########################################################################
    # adding URL for monthly Data
    # to display Yearly data
    url(r'^/year', ListView.as_view(queryset=Visitors.objects.raw("Select id,strftime('%Y',visiting_date) as m, count(strftime('%Y',visiting_date)) as c from home_visitors where visiting_date > date('now','-10 years')\
     group by strftime('%Y',visiting_date) order by visiting_date"), template_name="home/year.html")),

    # to display number of visitors from each state
    url(r'^/YstateYear', ListView.as_view(queryset=Visitors.objects.raw("Select LOWER(state) as id, count(LOWER(state)) \
    as count_data from home_visitors where date(visiting_date)>date('now','-10 years') \
    group by LOWER(state) order by count_data desc LIMIT 15 "), template_name="home/state.html")),

    # getting the Gender
    url(r'^/YgenderYear', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(gender) as gender_count, gender \
    from home_visitors  where date(visiting_date)>date('now','-10 years')\
    group by gender order by visiting_date"), template_name="home/gender.html")),

    # getting Visitors for checking new and returning visitors
    url(r'^/YvisitorYear', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(identity_no) as total \
    from home_visitors  where date(visiting_date)>date('now','-10 years')\
    group by identity_no order by visiting_date"), template_name="home/unique_visitor.html")),

    # getting all visitors for age group
    url(r'^/YageGroupYear', ListView.as_view(queryset=Visitors.objects.raw("Select id, year_of_birth,count(year_of_birth) as total \
    from home_visitors  where date(visiting_date)>date('now','-10 years')\
    group by year_of_birth"), template_name="home/ageGroup.html")),


    # card Type
    url(r'^/YcardTypeYear', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(type_of_id) as card_count, type_of_id as card \
    from home_visitors  where date(visiting_date)>date('now','-10 years')\
    group by type_of_id"), template_name="home/cardType.html")),


    # for real Time Data
    url(r'^/realTime', views.realTimeIndex),

    ##########################################################################
    # Real Time Data

    # adding URL for monthly Data
    # to display Yearly data
    url(r'^/recent', ListView.as_view(queryset=Visitors.objects.raw("Select id,strftime('%H:%M',time(visiting_date)) as minute,count(time(visiting_date)) as c from home_visitors where time(visiting_date) >  time(datetime('now','localtime'),'-10 minutes') and date(visiting_date) ==  date(datetime('now','localtime')) group by strftime('%M',visiting_date) order by visiting_date"), template_name="home/real.html")),

    # getting all visitors for age group
    url(r'^/GroupReal', ListView.as_view(queryset=Visitors.objects.raw("Select id, year_of_birth,count(year_of_birth) as total \
    from home_visitors  where time(visiting_date) > time(datetime('now','localtime'),'-10 minutes') and date(visiting_date) == date(datetime('now','localtime'))\
    group by year_of_birth"), template_name="home/ageGroup.html")),

     # getting the Gender
    url(r'^/GReal', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(gender) as gender_count, gender \
    from home_visitors  where time(visiting_date) > time(datetime('now','localtime'),'-10 minutes') and date(visiting_date) == date(datetime('now','localtime'))\
    group by gender order by visiting_date"), template_name="home/gender.html")),

    # getting Visitors for checking new and returning visitors
    url(r'^/VReal', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(identity_no) as total \
    from home_visitors where time(visiting_date) > time(datetime('now','localtime'),'-10 minutes') and date(visiting_date) == date(datetime('now','localtime'))\
    group by identity_no order by visiting_date"), template_name="home/unique_visitor.html")),

    # card Type
    url(r'^/CReal', ListView.as_view(queryset=Visitors.objects.raw("Select id, count(type_of_id) as card_count, type_of_id as card \
    from home_visitors where time(visiting_date) > time(datetime('now','localtime'),'-10 minutes') and date(visiting_date) == date(datetime('now','localtime'))\
    group by type_of_id"), template_name="home/cardType.html")),

    url(r'^/SReal', ListView.as_view(queryset=Visitors.objects.raw("Select LOWER(state) as id, count(LOWER(state)) \
    as count_data from home_visitors where time(visiting_date) > time(datetime('now','localtime'),'-10 minutes') and date(visiting_date) == date(datetime('now','localtime'))\
    group by LOWER(state) order by count_data desc LIMIT 15 "), template_name="home/state.html")),

]
