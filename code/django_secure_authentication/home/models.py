from django.db import models

# Create your models here.


class Visitors(models.Model):
    type_of_id = models.CharField(max_length=12,default='')
    identity_no = models.CharField(max_length=20,default='')
    name = models.CharField(max_length=25,default='')
    gender = models.CharField(max_length=1,default='')
    visiting_date = models.DateTimeField()
    year_of_birth = models.IntegerField()
    state = models.CharField(max_length=25,default='')
    type_of_entry = models.CharField(max_length=6,default='')

    def __str__(self):
        return self.name


class AadharDB(models.Model):
    identity_no = models.IntegerField()
    name = models.CharField(max_length=25,default='')
    gender = models.CharField(max_length=1,default='')
    year_of_birth = models.IntegerField()
    state = models.CharField(max_length=25,default='')
    finger_print = models.TextField(default='')
    phone_number = models.IntegerField()
    def __str__(self):
        return self.name
