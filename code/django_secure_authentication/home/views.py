from django.shortcuts import render
from django.http import HttpResponseRedirect
from home.models import Visitors
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.middleware.csrf import get_token
from home.models import AadharDB
import json
from django.http import JsonResponse

# Create your views here.


def index(request):
    return render(request, 'home/overview.html')


def realTimeIndex(request):
    return render(request, 'home/realTimeData.html')


@csrf_exempt
def addData(request):
    if(request.method == 'POST'):
        data = request.body.decode("utf-8")  # returns data in string
        qd = json.loads(data)  # converts to dict

        identity_no = qd["uid"]
        print(identity_no)
        name = qd['name']
        date = qd['date']
        gender = qd['gender']
        state = qd['state']
        yob = qd['yob']
        card_type = qd['card_type']
        entry_type = qd['entry_type']

        visitor_obj = Visitors(identity_no=identity_no,
                                   name=name,
                                   type_of_entry=entry_type,
                                   type_of_id=card_type,
                                   gender=gender, state=state, visiting_date=date,
                                   year_of_birth=yob)
        visitor_obj.save()

        return HttpResponseRedirect('/home')


@csrf_exempt
def retreiveData(request):
    data = request.body.decode("utf-8")  # returns data in string
    qd = json.loads(data)  # converts to dict
    uid = qd["uid"]
    data = {}

    #print(Visitors.objects.get(identity_no = 9888881302276))
    if AadharDB.objects.filter(identity_no=str(uid)).exists():
        for p in AadharDB.objects.raw("select id,identity_no, finger_print from home_aadharDB where identity_no = " + str(uid)):
            data['uid'] = p.identity_no
            data['fingerprint'] = p.finger_print
            data['gender'] = p.gender
            data['year_of_birth'] = p.year_of_birth
            data['state'] = p.state
            data['name'] = p.name
            data['phone_number'] = p.phone_number
            return JsonResponse(data)
    else:
        return HttpResponse("Not returning data")


@csrf_exempt
def addAdharData(request):
    data = request.body.decode("utf-8")  # returns data in string
    qd = json.loads(data)  # converts to dict
    identity_no = qd['uid']
    name = qd['name']
    gender = qd['gender']
    state = qd['state']
    yob = qd['yob']
    finger_print = qd['fingerprint']
    phone_number = qd['phonenumber']
    aadhar_obj = AadharDB(identity_no=identity_no,
                          name=name,
                          gender=gender, state=state,
                          year_of_birth=yob,
                          finger_print=finger_print, phone_number=phone_number)
    aadhar_obj.save()
    return HttpResponseRedirect('/home')
