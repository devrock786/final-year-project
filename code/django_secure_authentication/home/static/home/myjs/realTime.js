$(document).ready(function(){
    interval = null;
    realTimeData();
    interval = setInterval(realTimeData,20000)
})

function realTimeData() {
        $(".ct-chart").hide().load("/home/recent", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-ageGroup-chart").hide().load("/home/GroupReal", function () {
                $(this).fadeIn("slow");
        });


        $(".ct-gender-chart").hide().load("/home/GReal", function () {
                $(this).fadeIn("slow");
        });

        
         $(".ct-visitor-chart").hide().load("/home/VReal", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-card-chart").hide().load("/home/CReal", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-state-chart").hide().load("/home/SReal", function () {
                $(this).fadeIn("slow");
        });

}
