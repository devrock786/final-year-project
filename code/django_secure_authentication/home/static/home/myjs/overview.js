var interval = null;

$(document).ready(function () {
        clearInterval(interval);
        call_for_7_days();
        interval = setInterval(call_for_7_days, 20000);
});

$('#seven').click(function () {
        if (interval != null)
                clearInterval(interval);
        call_for_7_days();
        interval = setInterval(call_for_7_days, 20000);
});

$('#month').click(function () {
        if (interval != null)
                clearInterval(interval);
        call_for_month();
        interval = setInterval(call_for_month, 20000);
});

$('#year').click(function () {
        if (interval != null)
                clearInterval(interval);
        call_for_year();
        interval = setInterval(call_for_year, 20000);
});

function call_for_7_days() {
        //  sleep(0.1);
        $('#heading').text("Last 7 Days Data");

        $(".ct-chart").hide().load("/home/week", function () {
                $(this).fadeIn("slow");
        });
        
        $(".ct-state-chart").hide().load("/home/state", function () {
                $(this).fadeIn("slow");
        });


        $(".ct-gender-chart").hide().load("/home/gender", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-visitor-chart").hide().load("/home/visitor", function () {
                $(this).fadeIn("slow");
        });

        
         $(".ct-ageGroup-chart").hide().load("/home/ageGroupWeek", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-card-chart").hide().load("/home/cardType", function () {
                $(this).fadeIn("slow");
        });

}

function call_for_month() {
        $('#heading').text("Last 12 Months Data");

        $(".ct-chart").hide().load("/home/month", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-state-chart").hide().load("/home/MstateMonth", function () {
                $(this).fadeIn("slow");
        });


        $(".ct-gender-chart").hide().load("/home/MgenderMonth", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-visitor-chart").hide().load("/home/MvisitorMonth", function () {
                $(this).fadeIn("slow");
        });
         
          $(".ct-ageGroup-chart").hide().load("/home/MageGroupMonth", function () {
                $(this).fadeIn("slow");
        });

        
        $(".ct-card-chart").hide().load("/home/McardTypeMonth", function () {
                $(this).fadeIn("slow");
        });

         
}


function call_for_year() {
        $('#heading').text("Last 10 Years Data");

        $(".ct-chart").hide().load("/home/year", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-state-chart").hide().load("/home/YstateYear", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-gender-chart").hide().load("/home/YgenderYear", function () {
                $(this).fadeIn("slow");
        });

        $(".ct-visitor-chart").hide().load("/home/YvisitorYear", function () {
                $(this).fadeIn("slow");
        });

          $(".ct-ageGroup-chart").hide().load("/home/YageGroupYear", function () {
                $(this).fadeIn("slow");
        });
        
        $(".ct-card-chart").hide().load("/home/YcardTypeYear", function () {
                $(this).fadeIn("slow");
        });

}