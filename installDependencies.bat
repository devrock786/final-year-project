@echo off
:start
cls
pip install django
pip install opencv-python
pip install matplotlib
pip install apscheduler
pip install requests
pip install pillow
pip install pyzbar
pause
exit